> 文件清单如下：

D:\Lec\Lian_Panel
|
│  Lian_PanelData.do    // dofiles，附带本将所有实操命令
│  
├─Data
│      FE_BE_POLS.do
│      FE_mark.dta
│      FE_OLS_Negative.do
│      FE_OLS_Nodiff.do
│      FE_OLS_Positive.do
│      FE_OLS_Zero.do
│      FE_simudata.dta
│      FE_simudata_00.do
│      GTA_sample.dta
│      invest2.dta
│      nlswork.dta
│      xtcs.dta
│      xtlabor.dta
│      XT_FE_fig1.do
│      XT_FE_fig2.do
│      XT_FE_fig3.do
│      
├─Out
│      panel_Neg.png
│      panel_Nodiff.png
│      panel_Positive.png
│      panel_Pos_Scatter.png
│      panel_Zero.png
│      
└─refs
        Abadie_2017_adjust_SE.pdf
        Baltagi_2005.pdf
        Bruederl_Ludwig_FE_2015.pdf
        Cameron_2008_RES_bsClusterSE.pdf
        Cameron_2011_Clustered_se.pdf
        Cameron_2011_ClusterSE.pdf
        Cameron_2011_ClusterSE_PPT.pdf
        Cameron_2015_ClusterSE_JHR.pdf
        Flannery_2006.pdf
        Flannery_2006_FE.pdf
        Gormley_2014_RFS_FE.pdf
        Greene_2012.pdf
        Hisao_2003.pdf
        Horioka_2007_DPD.pdf
        Huang_2008_DPD.pdf
        Ibragimov_2010_clustse.pdf
        OLS_Partial_Corr.pdf
        panel_Neg.png
        Petersen-2009.pdf
        Rios_2015_SJ_15-3_FE.pdf
        SJ12-3-ReviewFE.pdf
        Sun_2004.pdf
        Thompson-2011.pdf
        Wooldridge_2010.pdf
        XT3_Hausman.pptx
        XT_FE_RE.pptx
        连玉君(2011)_Panel_Data.pdf
        连玉君_2012_消费文化.pdf
        连玉君_2014_Hausman检验.pdf


