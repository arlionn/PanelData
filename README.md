&#x1F34E; > &#x1F449;  点击右上角的【**Fork**】按钮，可以把这个项目完整复制到你的码云账号下，随时查看。 


---
### 《Stata直播丨直击面板数据模型 ——公开课》

---
> &#x23E9; 在线[观看](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38)：      
> 点击 <https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38>       
> 注册 [连享会直播间](http://lianxh.duanshu.com) 账号后即可免费观看。时长：1小时40分钟。
---

&emsp;

### 嘉宾简介


![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连玉君工作照100.JPG)  

**[连玉君](http://lingnan.sysu.edu.cn/node/151)** ，经济学博士，中山大学岭南学院副教授，博士生导师。已在 **China Economic Review**，**经济研究**，**管理世界**，**金融研究** 等期刊发表论文 60 余篇。连玉君老师团队一直积极分享 Stata 应用经验，创办了公众号「Stata连享会 (StataChina)」，开设了 [[Stata连享会-简书]](https://www.jianshu.com/u/69a30474ef33)，[[Stata连享会-知乎]](https://www.zhihu.com/people/arlionn) 两个专栏，累计阅读量超过 200 万人次。

&emsp; 

### 课程概览

> &#x1F4D9; [直播课展示](https://gitee.com/arlionn/Live)  &rarr; 包含部分课程课件和参考资料

本课程是 [[连玉君-动态面板数据模型 (2.2小时)]](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e) 和 [[连玉君-我的甲壳虫-经典论文精讲 (6小时)]](https://lianxh.duanshu.com/#/brief/course/c3f79a0395a84d2f868d3502c348eafc) 的先导课程，以介绍模型设定思路为主，实操部分请下载 「**Lian_Panel.rar**」压缩包自行演练。

课程中提及的各类模型的实操和综合应用会在 [[连玉君-我的甲壳虫(6小时)]](https://lianxh.duanshu.com/#/brief/course/c3f79a0395a84d2f868d3502c348eafc) 课程中详细讲解。

此外，若想加强研究设计能力，可以观看 [[连玉君-我的特斯拉-实证研究设计]](https://lianxh.duanshu.com/#/course/5ae82756cc1b478c872a63cbca4f0a5e) (2小时，包含 5 篇论文的重现资料，已经有 1000+ 人学习了该课程)。


&emsp;

#### 为何学习面板数据模型？

目前的实证分析中，基本上都是以「面板数据」为分析对象。好处很明显，一方面，随着样本量的增加，我们的统计推断会更加稳健；另一方面，面板数据同时提供了时序和截面的信息，使得我们既可以分析个体之间的截面差异，也可以分析他们时序动态变化。最为重要的是，使用面板数据还是缓解内生性问题的一个主要方法——我们可以控制那些不可观测的固定效应的影响。

本课程对面板数据模型进行整体简介，包括：固定效应模型 (FE)，随机效应模型 (RE)，二维固定效应模型 (Twoway FE)，聚类调整后的标准误，动态面板和面板门槛模型等。

> ![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-直击面板数据二维码.png)    
> 扫码观看视频


### 课程主题

本次直播主要包括如下几个主题：

- 简介：面板数据结构、优势和挑战
- 什么是「固定效应」？辛普森悖论
- 一维和二维固定效应模型
- 估计方法对比分析：POLS，DVLS，Within-FE
- 聚类标准误：一维聚类和多维聚类
- 实证分析中的主要陷阱
- 动态面板和面板门限模型简介

### 获取课程电子包

你可以点击主页右上方的【克隆/下载&rarr;下载zip】，以便下载本仓库的压缩包；也可以申请一个 [码云](https://gitee.com) 账号，然后点击本项目右上角的【Fork】按钮，这样就可以直接把这个仓库「叉」到你的账号下了，随后我这边更新后，你只需要同步一下就可以看到所有的文件了。


> **实操：Stata dofiles/Data/Papers:** 

下载地址：<https://pan.baidu.com/s/1Ri38Xyz_TnFzLNYgsOes4w> (百度云盘，无解压码)。

&emsp; 

&#x1F34E; &#x1F34F;


&emsp;


&emsp;


![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

> [主页](https://www.lianxh.cn/news/46917f1076104.html)  || [视频](http://lianxh.duanshu.com) || [推文](https://www.lianxh.cn/news/d4d5cd7220bc7.html) || [知乎](https://www.zhihu.com/people/arlionn/) || [Bilibili 站](https://space.bilibili.com/546535876) 

---

## 1. 计量专题

&emsp;

> [最新上线](https://gitee.com/arlionn/collections/278581) | [公开课](https://gitee.com/arlionn/collections/278579) | [小专题](https://gitee.com/arlionn/collections/1257)

&emsp; 

> ### ${\color{blue}{\blacktriangleright}}$ [课程主页](https://www.lianxh.cn/news/46917f1076104.html)

&emsp;

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/MayunKC-001.png)](https://www.lianxh.cn/news/46917f1076104.html)

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/MayunKC-002.png)](https://www.lianxh.cn/news/46917f1076104.html)

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/MayunKC-003.png)](https://www.lianxh.cn/news/46917f1076104.html)

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/MayunKC-004.png)](https://www.lianxh.cn/news/46917f1076104.html)

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/MayunKC-005.png)](https://www.lianxh.cn/news/46917f1076104.html)

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/MayunKC-006.png)](https://www.lianxh.cn/news/46917f1076104.html)

> ### ${\color{red}{\blacktriangleright}}$ [计量专题](https://gitee.com/arlionn/Course)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

&emsp;

## 2. 公开课/专题课

### 公开课 
- 侯丹丹，Stata软件及计量基础，6 次课 x 2h，[去听课](https://lianxh.duanshu.com/#/brief/course/49c3dbf11982456db9be1b5f595285dc)，[课件](https://gitee.com/arlionn/stataopen)
- 连玉君，直击面板数据模型，1.4h，[去听课](http://lianxh-pc.duanshu.com/course/detail/7d1d3266e07d424dbeb3926170835b38)，[B站视频](https://www.bilibili.com/video/BV1oU4y187qY)，[课件](https://gitee.com/arlionn/PanelData)
- 连玉君，Stata 33 讲，15min/讲. [去听课](http://lianxh-pc.duanshu.com/course/detail/b22b17ee02c24015ae759478697df2a0)，[B站视频](https://space.bilibili.com/546535876/channel/detail?cid=160748)，[课件](https://gitee.com/arlionn/stata101)  
- 连玉君，论文复现：why？what？How？，2h，[去听课](https://www.lianxh.cn/news/3544e941a02d8.html)，[B站视频](https://www.bilibili.com/video/BV1D54y1n7Fa)，[课件](https://gitee.com/arlionn/rep)
- 龙志能，Stata小白的取经之路，1.5h，[去听课](https://lianxh.duanshu.com/#/brief/course/137d1b7c7c0045e682d3cf0cb2711530)，[课件](https://gitee.com/arlionn/StataBin)
- 涂冰倩，微观数据库清理经验分享，2h，[去听课](https://gitee.com/arlionn/dataclean/blob/master/%E5%85%AC%E5%BC%80%E8%AF%BE%EF%BC%9A%E6%B6%82%E5%86%B0%E5%80%A9-%E5%BE%AE%E8%A7%82%E6%95%B0%E6%8D%AE%E5%BA%93%E6%B8%85%E7%90%86%E7%BB%8F%E9%AA%8C%E5%88%86%E4%BA%AB.md#https://gitee.com/link?target=https%3A%2F%2Fwww.lianxh.cn%2Fblogs%2F16.html)，[课件](https://gitee.com/arlionn/dataclean)
- 游万海，实证分析中的数据可视化，2h，[去听课](https://lianxh.duanshu.com/#/brief/course/da1a75bc3acc4e238f489af3367efa26)，[课件](https://gitee.com/arlionn/Rplot)
- 杨海生，如何玩转空间计量？，1.5h，[去听课](https://lianxh.duanshu.com/#/brief/course/675437c0c52f4e48947531286cef1e87)，[课件](https://gitee.com/arlionn/SP/blob/master/%E5%85%AC%E5%BC%80%E8%AF%BE-%E6%9D%A8%E6%B5%B7%E7%94%9F-%E7%8E%A9%E8%BD%AC%E7%A9%BA%E9%97%B4%E8%AE%A1%E9%87%8F.md)
- 范巧，空间计量经济学的基本框架及分析范式，[去听课](https://lianxh.duanshu.com/#/brief/course/6b294c08454042ce860e43363bbeed5b)，[课件](https://gitee.com/arlionn/SP/blob/master/%E5%85%AC%E5%BC%80%E8%AF%BE-%E8%8C%83%E5%B7%A7-%E7%A9%BA%E9%97%B4%E8%AE%A1%E9%87%8F%E7%BB%8F%E6%B5%8E%E5%AD%A6%E5%9F%BA%E6%9C%AC%E6%A1%86%E6%9E%B6%7C%E5%88%86%E6%9E%90%E8%8C%83%E5%BC%8F.md)
- 范巧，空间计量建模与高质量论文撰写，[去听课](https://lianxh.duanshu.com/#/brief/course/35252658d4a046a0b0b74ceea1f8e0ee)，[课件](https://gitee.com/arlionn/SP/blob/master/%E5%85%AC%E5%BC%80%E8%AF%BE-%E8%8C%83%E5%B7%A7-%E7%A9%BA%E9%97%B4%E8%AE%A1%E9%87%8F%E7%BB%8F%E6%B5%8E%E5%AD%A6%E5%9F%BA%E6%9C%AC%E6%A1%86%E6%9E%B6%7C%E5%88%86%E6%9E%90%E8%8C%83%E5%BC%8F.md)
- 龙志能，如何高效运用搜索解决问题？，1h，[去听课](https://lianxh.duanshu.com/#/brief/course/ac14b768d2314d43a8a805205a45d3e3)
- 王昆仑，双重差分方法的新进展-交错型DID，2h，[去听课](https://lianxh.duanshu.com/#/brief/course/9f83a4513f7f409b96e40a1bd0c79379)
- [连享会码云：100多个精选计量项目](https://www.lianxh.cn/news/944a69d75cec9.html) |  [新浪视频](https://weibo.com/tv/show/1034:4479228373303338)
- [五分钟 Markdown / Markdown 幻灯片](https://gitee.com/arlionn/md) | [新浪视频](https://weibo.com/tv/show/1034:4484204327796746)
- [连老师给你的-听课建议](https://www.lianxh.cn/news/69706e871c9ad.html)

### 专题小课
- 连玉君，我的特斯拉-实证研究设计，2.2h，[去听课](https://lianxh.duanshu.com/#/course/5ae82756cc1b478c872a63cbca4f0a5e)，[课件](https://gitee.com/arlionn/Live/tree/master/%E6%88%91%E7%9A%84%E7%89%B9%E6%96%AF%E6%8B%89-%E5%AE%9E%E8%AF%81%E7%A0%94%E7%A9%B6%E8%AE%BE%E8%AE%A1-%E8%BF%9E%E7%8E%89%E5%90%9B)
- 连玉君，我的甲壳虫-论文精讲与重现，6h，[去听课](https://lianxh.duanshu.com/#/brief/course/c3f79a0395a84d2f868d3502c348eafc)，[课件](https://gitee.com/arlionn/paper101)
- 连玉君，动态面板数据模型，2.2h，[去听课](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e)，[课件](https://gitee.com/arlionn/Live/tree/master/%E8%BF%9E%E7%8E%89%E5%90%9B-%E5%8A%A8%E6%80%81%E9%9D%A2%E6%9D%BF%E6%A8%A1%E5%9E%8B)
- 游万海，Stata 数据清洗之实战操作 (第一季)，2h，[去听课](https://lianxh.duanshu.com/#/brief/course/c5193f0e6e414a7e889a8ff9aeb4aaef)，[主页](https://gitee.com/arlionn/dataclean)
- 游万海，Stata 数据清洗之实战操作 (第二季)，2h，[去听课](https://lianxh.duanshu.com/#/brief/course/23924488072b4e458ec3bb0a830b187f)，[主页](https://gitee.com/arlionn/dataclean)
- 游万海，分位数回归，2h，[去听课](https://lianxh.duanshu.com/#/brief/course/f0bfb3102ada48969966c92123a7ebf0)

&emsp;

## 3. 资源分享

连享会主要通过三个渠道分享实证分析经验。

> **主页**：[www.lianxh.cn](https://www.lianxh.cn)  
  - 目前已分享 1000 余篇推文，包括 Stata/R/Python 等软件使用经验，[各类](https://www.lianxh.cn/blogs.html) 计量模型，内生性、DID、RDD 专题等。
  - **人手一份：Stata 101**，[PDF下载](https://file.lianxh.cn/KC/Slides/lianxh_Stata101.pdf), [HTML下载](https://file.lianxh.cn/KC/Slides/lianxh_Stata101.html) | [Stata Journal](https://www.lianxh.cn/news/12ffe67d8d8fb.html)
> **b 站**：<https://space.bilibili.com/546535876> 
  - 亦可在 b 站搜索关键词 **连享会**
  - 内容：浏览量超过 10 w 的公开课视频，包括 [Stata 33 讲](https://space.bilibili.com/546535876/channel/detail?cid=160748)、[直击面板数据模型](https://www.bilibili.com/video/BV1oU4y187qY)、[论文复现：why？what？How？](https://www.bilibili.com/video/BV1D54y1n7Fa) 等。
> **码云仓库**：<https://gitee.com/arlionn>
  - 连享会收集整理的代码仓库，分为 30 多个 [专题](https://gitee.com/arlionn/collections)，500+ 个 [仓库](https://gitee.com/arlionn/projects)，涉及论文复现代码、RDD，DID，Panel，SCM，Lasso 等最新方法实现代码等。
  - 用法：申请一个码云账号，然后点击仓库右上角的 **[Fork](https://gitee.com/arlionn/list#)** 按钮，将喜欢的仓库克隆到你的账号下，随后点击仓库名称右边的 $\curvearrowright$ 按钮强制同步。

### Data
- [CSMAR-国泰安](http://www.gtarsc.com/#/datacenter/singletable) | [Wind-万德](https://www.wind.com.cn/Default.html) | [Resset-锐思](http://www.resset.cn/databases)
- [常用数据库](https://www.lianxh.cn/news/0b65fd5165c2c.html) | [人文社科开放数据库](https://www.lianxh.cn/news/6f06c914acde8.html) | [知乎-Data](https://www.zhihu.com/question/20179699/answer/681756635)
- [徐现祥教授-IRE-官员交流、方言等](https://www.lianxh.cn/news/8c9f81a5f19ee.html)

### Papers - 学术论文复现
- [论文重现网站大全](https://www.lianxh.cn/news/e87e5976686d5.html)
- Stata论文重现：[Harvard][harvd] | [Top 期刊论文](https://ejd.econ.mathematik.uni-ulm.de/) | [JFE][jfe]  | [github][git1] 
- 学者主页：[Angrist][Ang1] | [Acemoglu][acem]  | [Levine][ross] | [Duflo][Duflo] | [Imbens](https://scholar.harvard.edu/imbens/software) | [Chetty](http://www.rajchetty.com/)

[harvd]:https://dataverse.harvard.edu/dataverse
[jfe]:http://jfe.rochester.edu/data.htm
[Ang1]:http://economics.mit.edu/faculty/angrist/data1/data
[acem]:http://economics.mit.edu/faculty/acemoglu/data
[ross]:http://faculty.haas.berkeley.edu/ross_levine/papers.htm
[duflo]:http://economics.mit.edu/faculty/eduflo/papers
[git1]:https://github.com/search?utf8=%E2%9C%93&q=stata&type=

[yahoogit]:https://search.yahoo.com/search;_ylt=AwrBT8di2LBZqyEAuG9XNyoA;_ylc=X1MDMjc2NjY3OQRfcgMyBGZyA3lmcC10LTQ3MwRncHJpZAMEbl9yc2x0AzAEbl9zdWdnAzAEb3JpZ2luA3NlYXJjaC55YWhvby5jb20EcG9zAzAEcHFzdHIDBHBxc3RybAMwBHFzdHJsAzE0BHF1ZXJ5A3N0YXRhJTIwZ2l0aHViBHRfc3RtcAMxNTA0NzYxODcz?p=stata+github&fr2=sb-top&fr=yfp-t-473&fp=1


&emsp;


## 4. 关于我们

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。[直播间](http://lianxh.duanshu.com) 有很多视频课程，可以随时观看。
- [连享会-主页](https://www.lianxh.cn) 和 [知乎专栏](https://www.zhihu.com/people/arlionn/)，500+ 推文，实证分析不再抓狂；[Bilibili 站](https://space.bilibili.com/546535876) 有视频大餐。

><font color=red>New！</font> **`lianxh` 命令发布了！**    

- 随时搜索连享会推文、Stata 资源，安装命令如下：  
  &emsp; `. ssc install lianxh`  
- 使用详情参见帮助文件 (有惊喜)：   
  &emsp; `. help lianxh`

> 连享会小程序：扫一扫，看推文，看视频……

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会小程序二维码180.png)](https://www.lianxh.cn/news/46917f1076104.html)

> 扫码加入连享会微信群，提问交流更方便

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-学习交流微信群001-150.jpg)


![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-草料主页-一码平川600.png)